# README #

### ¿Para qué es este repositorio? ###

* Este repositorio contiene los ejemplos básicos de programación en ensamblador ARM que aparecen en el guión de la práctica de la asignatura de Tecnología de Computadores de los Grados en Informática de la Universidad de Málaga

### ¿Que hacer con ellos? ###

* Descargarlos y simularlos con ARMSim# o ensamblarlos y ejecutarlos en la Raspberry Pi
